import { useEffect, useState, useContext } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { Fragment } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import { Link, Route } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";
import { Row, Col } from "react-bootstrap";

// import the hooks that are needed in our page

export default function Register() {
  // Create 3 new state where we will store the value from input of the email, password and confirmPassword

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [mobileNo, setMobile] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("null");
  const [isActive, setIsActive] = useState(false);

  const { user, setUser } = useContext(UserContext);

  const navigate = useNavigate();

  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      mobileNo !== "" &&
      email !== "" &&
      password !== "" &&
      confirmPassword !== "" &&
      password === confirmPassword
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, mobileNo, email, password, confirmPassword]);

  function register(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        mobileNo: mobileNo,
        email: email,
        password: password,
      }),
    })
      .then((result) => result.json())
      .then((data) => {
        console.log(data);
        if (data) {
          Swal.fire({
            title: "Registration Successful!",
            icon: "success",
            text: "You can now Login!",
          });
          navigate("/login");
        } else {
          // localStorage.setItem("token", data.auth);
          // retrieveUserDetails(localStorage.getItem("token"));
          Swal.fire({
            title: "Registration Failed!",
            icon: "error",
            text: "The email is already existed!",
          });
        }
      });

    // localStorage.setItem("email", email);
    // setUser(localStorage.getItem("email"));
    // alert("Congratulations, you are now registered on our website!");

    // setEmail("");
    // setPassword("");
    // setConfirmPassword("");

    // navigate("/");
  }

  return user ? (
    <Navigate to="*" />
  ) : (
    <Fragment>
      <h1 className="text-center mt-5">Register</h1>
      <Row className="col-6 offset-3 offset-md-0 offset-md-3 col-lg-4 offset-lg-4">
        <Col>
          <Form className="mt-5" onSubmit={(event) => register(event)}>
            <Form.Group className="mb-3" controlId="formBasicFirstName">
              <Form.Label>First Name</Form.Label>
              <Form.Control
                type="String"
                placeholder="Enter first name"
                value={firstName}
                onChange={(event) => setFirstName(event.target.value)}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicLastName">
              <Form.Label>Last Name</Form.Label>
              <Form.Control
                type="String"
                placeholder="Enter last name"
                value={lastName}
                onChange={(event) => setLastName(event.target.value)}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicMobileNo">
              <Form.Label>Mobile No.</Form.Label>
              <Form.Control
                type="String"
                placeholder="Enter mobile no."
                value={mobileNo}
                onChange={(event) => setMobile(event.target.value)}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                required
                onChange={(event) => setPassword(event.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicConfirmPassword">
              <Form.Label>Confirm Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Confirm your Password"
                value={confirmPassword}
                required
                onChange={(event) => setConfirmPassword(event.target.value)}
              />
            </Form.Group>

            {/* in this code block, we do conditional rendering depending on the state of our isActive */}
            {isActive ? (
              <Button variant="primary" type="submit">
                Submit
              </Button>
            ) : (
              <Button variant="danger" type="submit" disabled>
                Submit
              </Button>
            )}
          </Form>
        </Col>
      </Row>
    </Fragment>
  );
}
