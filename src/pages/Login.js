import { useEffect, useContext, useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { Fragment } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import { Col, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import UserContext from "../UserContext.js";
import Swal from "sweetalert2";
// import the hooks that are needed in our page

export default function Login() {
  // Create 3 new state where we will store the value from input of the email, password and confirmPassword

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  // Button useState
  const [isActive, setIsActive] = useState(false);

  const navigate = useNavigate();

  // const [user, setUser] = useState(localStorage.getItem("email"));

  // Allows us to consume the UserContext object and it's properties for user validation

  const { user, setUser } = useContext(UserContext);

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  function login(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
      method: "Post",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((result) => result.json())
      .then((data) => {
        console.log(data);
        if (data === false) {
          Swal.fire({
            title: "Authentication Failed!",
            icon: "error",
            text: "Please Try Again!",
          });
        } else {
          localStorage.setItem("token", data.auth);
          retrieveUserDetails(localStorage.getItem("token"));
          Swal.fire({
            title: "Authentication Successful!",
            icon: "success",
            text: "Welcome to Zuitt!",
          });
          navigate("/");
        }
      });

    // localStorage.setItem("email", email);
    // console.log(localStorage.getItem("email"));
    // setUser(localStorage.getItem("email"));
    // alert("You are now logged in.");
    // setEmail("");
    // setPassword("");

    // navigate("/");
  }
  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((result) => result.json())
      .then((data) => {
        console.log(data);
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  };

  return user ? (
    <Navigate to="/*" />
  ) : (
    <Fragment>
      <Row className="col-4 offset-4">
        <Col>
          <h1 className="text-center mt-5">Login</h1>
          <Form className="mt-5" onSubmit={(event) => login(event)}>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                required
                onChange={(event) => setPassword(event.target.value)}
              />
            </Form.Group>

            {/* in this code block, we do conditional rendering depending on the state of our isActive */}
            {isActive ? (
              <Button variant="success" type="submit">
                Submit
              </Button>
            ) : (
              <Button variant="success" type="submit" disabled>
                Login
              </Button>
            )}
          </Form>
        </Col>
      </Row>
    </Fragment>
  );
}
