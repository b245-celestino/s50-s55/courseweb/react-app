import { Fragment } from "react";
import { Link } from "react-router-dom";
import { Row, Col } from "react-bootstrap";

export default function NotFound() {
  return (
    <Fragment>
      <Row className="text-center mt-5">
        <Col>
          <h1>Page Not Found.</h1>

          <p>
            Go back to the <Link to="/">homepage</Link>
          </p>
        </Col>
      </Row>
    </Fragment>
  );
}
