import "./App.css";
import { useEffect, useState } from "react";
import AppNavBar from "./components/AppNavBar.js";
import Home from "./pages/Home";
import { Container } from "react-bootstrap";
import Courses from "./pages/Courses";
import Register from "./pages/Register";
import Login from "./pages/Login";
// import Banner from "./Banner.js";
// import Highlights from "./Highlights.js";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Logout from "./pages/Logout";
import NotFound from "./pages/PageNotFound";
import { UserProvider } from "./UserContext";
import CourseView from "./components/CourseView";

function App() {
  const [user, setUser] = useState(null);

  useEffect(() => {
    console.log(user);
  }, [user]);

  const unSetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((result) => result.json())
      .then((data) => {
        console.log(data);

        if (localStorage.getItem("token") !== null) {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
          });
        } else {
          setUser(null);
        }
      });
  }, []);

  // Storing information in a context object is done by providing the information using the corresponding "Provider" and passing information thru the prop values;
  // all information/data provided to the Provider component can be access later on from the context object properties
  return (
    <UserProvider value={{ user, setUser, unSetUser }}>
      <Router>
        <AppNavBar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="courses" element={<Courses />} />
          <Route path="login" element={<Login />} />
          <Route path="register" element={<Register />} />
          <Route path="logout" element={<Logout />} />
          <Route path="*" element={<NotFound />} />
          <Route path="/course/:courseId" element={<CourseView />} />
        </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
