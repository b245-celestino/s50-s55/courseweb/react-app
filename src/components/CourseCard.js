import { Button } from "react-bootstrap";
import { Row, Col } from "react-bootstrap";
import { Card } from "react-bootstrap";
import { useEffect, useState, useContext } from "react";
import UserContext from "../UserContext";
import { Link } from "react-router-dom";
import { useParams } from "react-router-dom";

export default function CourseCard({ courseProp }) {
  console.log(courseProp);

  const { _id, name, description, price } = courseProp;

  // The "course" in the CourseCard component is called a prop

  // Use the state hook for this component to be able to store its state
  // States are used to keep track of information related to individual components
  //  const [getter, setter] = useState(initialGetterValue)

  const [enrollees, setEnrollees] = useState(0);
  const [availableSeats, setSeats] = useState(30);

  // add new state that will declare whether the button is disable or not

  const [isDisabled, setIsDisable] = useState(false);

  const { user } = useContext(UserContext);

  // Initial value of enrollees state
  console.log(enrollees);

  // if you want to change/reassign the value of the state
  // setEnrollees(1);
  // console.log(enrollees);
  function enroll() {
    setEnrollees(enrollees + 1);
    setSeats(availableSeats - 1);
    if (availableSeats === 0 && enrollees === 0) {
      alert("Congratulations for making it to the cut!");
      setEnrollees(30);
      setSeats(0);
    } else if (availableSeats === 1 && enrollees === 29) {
      alert("Congratulations for making it to the cut!");
    }
  }

  // Define a "useEffect" hook to have the "CourseCard" component do perform a certain task

  // syntax:
  // useEffect(sideEffect/function. [dependencies])

  // sideEffect/function - it will run on the first load and will reload depending

  useEffect(() => {
    if (availableSeats === 0) {
      setIsDisable(true);
    }
  }, [availableSeats]);

  return (
    <Row className="mt-5 mx-2 col-12 col-lg-2">
      <Col>
        <Card>
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>Php {price}</Card.Text>
            <Card.Subtitle>Enrollees:</Card.Subtitle>
            <Card.Text>{enrollees}/30</Card.Text>
            <Card.Subtitle>Available Seats:</Card.Subtitle>
            <Card.Text>{availableSeats}</Card.Text>

            {user ? (
              <Button as={Link} to={`/course/${_id}`} disabled={isDisabled}>
                See more details
              </Button>
            ) : (
              <Button as={Link} to="/login">
                Login
              </Button>
            )}
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
