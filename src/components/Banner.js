// destructuring
import { Button } from "react-bootstrap";
import { Container, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Banner() {
  return (
    <Row className="mt-5">
      <Col className="text-center pt-1">
        <h1>Zuitt Coding Bootcamp</h1>
        <p className="pt-1">Opportunities for everyone, everywhere.</p>
        <Button className="pt-1" as={Link} to="/courses">
          Enroll Now
        </Button>
      </Col>
    </Row>
  );
}
