// to create a react app.
//  npx create-react-app project-name

//  to run our react application
// npm start

// files to be removed:
// From folder src:
// App.test.js
// index.css
// reportWebVital.js
// logo.svg

// We need to delete all the importations of the said files

// The 'import' statement allows us to use the code/export modules from other files similar to how we use the 'require' function in Node.js

//  React JS it applies the concepts of rendering and mounting in order to display and create components

// Rendering refers to the process of calling/invoking a component returning set of instructions creating DOM

// Mounting is when REACT JS "renders or displays" the component the initial DOM based on the instructions
